extends Camera2D
var player_1_pos
var player_2_pos
var new_pos

signal player_2_dead

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	# Initialization here
	
func _process(delta):

	player_1_pos = get_node("../player1").position

	player_2_pos = get_node("../player2").position
	
	new_pos = (player_1_pos + player_2_pos) / 2
	
	self.position = Vector2(new_pos)
	
	var distance = player_1_pos.distance_to(player_2_pos) * 2
	var zoom_factor = distance * 0.00063
	var minimum_zoom = Vector2(1, 1)
	var new_zoom = Vector2(1, 1) * zoom_factor / 2
	if new_zoom < Vector2(1, 1):
		self.zoom = Vector2(1, 1)
	else:
		self.zoom = new_zoom
	
	# self.zoom = Vector2(1,1)
	pass
