extends TextureRect

var alreadyseen = 0
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	if alreadyseen == 1:
		get_tree().change_scene("res://Scenes/Floor_is_Lava/level-1-lava.tscn")
	get_node("/root/AnimationPlayer").current_animation = "Cutscene 1"
	pass # Replace with function body.

func _finsihed_anim():
	alreadyseen = 1
	print("Finished Animation!")
	get_tree().change_scene("res://Scenes/Floor_is_Lava/level-1-lava.tscn")
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
