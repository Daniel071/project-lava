extends KinematicBody2D

const GRAVITY = 1600.0
const WALK_SPEED = 400

var velocity = Vector2()
var on_ground = false
var scene_list = ["res://Scenes/Level 1.tscn", "res://Scenes/Level 2.tscn"]
var current_scene = 0
var ignoreThis = 0

func _on_ready():
	pass

func _physics_process(delta):
	if is_on_floor():
		on_ground = true
	else:
		on_ground = false
	
	velocity.y += delta * GRAVITY

	if Input.is_action_pressed("player-1-left"):
		velocity.x = -WALK_SPEED
	elif Input.is_action_pressed("player-1-right"):
		velocity.x =  WALK_SPEED
	else:
		velocity.x = 0
		
	if Input.is_action_just_pressed("player-1-up") and is_on_floor():
		velocity.y = -780
	
	if position.y > 1500:
		position = Vector2(0, -1);
		
	if is_on_floor() and not Input.is_action_pressed("player-1-up"):
		velocity.y = 100

	# We don't need to multiply velocity by delta because "move_and_slide" already takes delta time into account.

	# The second parameter of "move_and_slide" is the normal pointing up.
	# In the case of a 2D platformer, in Godot, upward is negative y, which translates to -1 as a normal.
	move_and_slide(velocity, Vector2(0, -1))

func _on_DoorArea_body_entered(body):
	if ignoreThis == 1:
		current_scene = current_scene + 1
		get_tree().change_scene(scene_list[current_scene])
		print("Collision detected!")
	else:
		ignoreThis = 1
		pass
	

func _on_Area2D_area_entered(area):
	if not get_node("/root").has_node("mainlevel/player2"):
		print("Player 2 does not exist")
		# print(get_node("/root").print_tree())
		get_tree().change_scene("res://Scenes/UI/Failed-level.tscn")
	else:
		print("Player 2 does exist")
		# print(get_node("/root").print_tree())
		get_node("../Camera2D").queue_free()
		get_node("../player1").queue_free()
	pass # Replace with function body.
