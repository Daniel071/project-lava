# Project Lava
## Description:
Project Lava is an open-source game that has 2-player support and controller support,
it is available in all platforms including Windows, Linux and MacOS

It comes with two different game modes:
1. Platform Mode (In Beta)

  - A one player platformer where you need to collect coins and reach the end of the level!
  <br></br>
2. Lava mode (In Beta)

  - A two player platformer where you have to reach the door before the lava reaches the top!
  <br></br>

###### Disclaimer: This game is still in development and may have many bugs and lack features, please be patient!

---
# How to download and run
## Windows:
<img src="https://upload.wikimedia.org/wikipedia/commons/5/5f/Windows_logo_-_2012.svg" alt="Windows 10 Logo" width="150"/> <img src="https://upload.wikimedia.org/wikipedia/en/1/14/Windows_logo_-_2006.svg" alt="Windows 7 Logo" width="150"/>  
1. Download the windows build here:
https://gitlab.com/Daniel071/project-lava/-/releases
2. Double click on the exe file
3. If Windows Smartscreen shows up, click *'More info'* and *'Run anyway'*
###### Don't worry if windows smartscreen says *'Your PC might be at risk'*, this is only triggered as this program is not signed

## Linux and BSD:
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Tux.svg/2000px-Tux.svg.png" alt="Linux Logo" width="150"/>

1. Download the latest X11/Linux build here:
https://gitlab.com/Daniel071/project-lava/-/releases
2. Give this program executable privileges:
`chmod -x /path/to/file`
3. Run the program

## MacOS:
<img src="https://upload.wikimedia.org/wikipedia/commons/2/22/MacOS_logo_%282017%29.svg" alt="MacOS Logo" width="150"/>

1. Download the mac build here:
https://gitlab.com/Daniel071/project-lava/-/releases
2. Make sure you right click the executable and click open.
3. On the dialog that says "Project Lava is from an unidentified developer.", click 'Open'.
